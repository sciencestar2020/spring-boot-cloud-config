package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigClientStarter {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClientStarter.class,args);
        System.out.println("Config 客户端启动.............");
    }
}

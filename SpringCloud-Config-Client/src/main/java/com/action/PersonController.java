package com.action;

import com.bean.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/person")
public class PersonController {

    @Autowired
    private UserInfo userInfo;

    @RequestMapping(value = "/getUserInfo")
    public String getUserInfo(){
        return "Welcome:"+userInfo.getUsername();
    }
}
